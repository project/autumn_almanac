<?php
/* Node date style and username  - Used in node.tpl*/

function autumn_almanac_preprocess_node(&$vars) {

$node = $vars['node'];
$vars['date'] = format_date($node->created, 'custom', "l, F j, Y");
$vars['user_name'] = $vars['name'];
}


/* These are needed for the body classes. Do not remove */

function autumn_almanac_preprocess_page(&$vars, $hook) {

    $classes = split(' ', $vars['body_classes']);
    
     if(empty($vars['footer_right'])){
        $classes[] = t('no-footer-right');
    } else{
        $classes[] = t('has-footer-right');
    }
    
    if(empty($vars['footer_middle'])){
        $classes[] = t('no-footer-middle');
    } else{
        $classes[] = t('has-footer-middle');
    }
    
     if(empty($vars['footer_left'])){
        $classes[] = t('no-footer-left');
    } else{
        $classes[] = t('has-footer-left');
    }
  
    
    $vars['body_classes_array'] = $classes;
    $vars['body_classes'] = implode(' ', $classes);
    
}

/* Breadcrumb */


function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
// uncomment the next line to enable current page in the breadcrumb trail
 $breadcrumb[] = drupal_get_title();
    return '<div class="breadcrumb">'. implode(' &gt; ', $breadcrumb) .'</div>';
  }
}

/**

Below changes the layout of the search box


* Override or insert PHPTemplate variables into the search_theme_form template.
*
* @param $vars
*   A sequential array of variables to pass to the theme template.
* @param $hook
*   The name of the theme function being called (not used in this case.)
*/


function autumn_almanac_preprocess_search_theme_form(&$vars, $hook) {

  // Modify elements of the search form
  $vars['form']['search_theme_form']['#title'] = t('');
 
  // Set a default value for the search box
 $vars['form']['search_theme_form']['#value'] = t('Search this Site');
 
  // Add a custom class to the search box
  $vars['form']['search_theme_form']['#attributes'] = array('class' => 'NormalTextBox txtSearch',
  'onfocus' => "if (this.value == 'Search this Site') {this.value = '';}",
  'onblur' => "if (this.value == '') {this.value = 'Search this Site';}");
 
  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('Search');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);

  $vars['form']['submit']['#type'] = 'image_button';
  $vars['form']['submit']['#src'] =  'sites/all/themes/autumn_almanac/images/search.gif';

  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}



function autumn_almanac_preprocess_search_block_form (&$vars, $hook) {

  // Modify elements of the search form
  $vars['form']['search_block_form']['#title'] = t('');
 
  // Set a default value for the search box
   //$vars['form']['search_block_form']['#value'] = t('Search this Site');
 
  // Add a custom class to the search box
  //$vars['form']['search_block_form']['#attributes'] = array('class' => 'NormalTextBox txtSearch',
  //'onfocus' => "if (this.value == 'Search this Site') {this.value = '';}",
  //'onblur' => "if (this.value == '') {this.value = 'Search this Site';}");
 
  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('Search');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_block_form']['#printed']);
  $vars['search']['search_block_form'] = drupal_render($vars['form']['search_block_form']);

  $vars['form']['submit']['#type'] = 'image_button';
  $vars['form']['submit']['#src'] =  'sites/all/themes/autumn_almanac/images/search.gif';

  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}