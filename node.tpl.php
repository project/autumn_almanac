<?php // $Id$  ?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">

<?php print $picture ?>

<?php if (!$page): ?>
  <span class="node-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></span>
<?php endif; ?>

  <div class="meta">
  <?php if ($submitted): ?>
    <span class="submitted">Posted by <?php print $user_name ?> on <span class="submitted-date"><?php print $date ?></span></span>
  <?php endif; ?>

  <?php if ($terms): ?>
    <div class="terms terms-inline">Tags: <?php print $terms ?></div>
  <?php endif;?>
  </div>

  <div class="content">
    <?php print $content ?>
  </div>
<div class="node-bottom-links">
  <?php print $links; ?>
  </div>

</div>