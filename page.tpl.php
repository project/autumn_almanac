<?php // $Id$  ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  
    <!--[if IE 8]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie8.css";</style>
<![endif]-->
    <!--[if IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie7.css";</style>
<![endif]-->
<!--[if IE 6]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie6.css";</style>
<![endif]-->

  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
  

<script type="text/javascript">
$(document).ready( function(){
  $('.node').corners("10px transparent");

});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('div.examples').pngFix( );
	});
</script>


 
  
</head>

<body class="<?php print $body_classes; ?>">

<div id="wrapper">

    <div id="header">
	
	    <?php if (!empty($breadcrumb)): ?>
	    <div id="breadcrumb">
	 
	    <?php print $breadcrumb; ?>
	  
	    </div>
	    <?php endif; ?>
	  
      <div id="logo-title">
	  
	     <?php if (!empty($logo)): ?>
		  <div id="logo" class="pngfix">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
		  </div>
        <?php endif; ?>

        <div id="site-name">

       <div id="site-name-title">
              <?php if (!empty($site_name)): ?>
            <h1>
              <?php print $site_name; ?>
            </h1>
          <?php endif; ?>
			
			</div>
  
        </div> <!-- /site name -->
      </div> <!-- /logo-title -->
    </div> <!-- /header -->
	
	  
        
       <div id="navigation" class="menu <?php if (!empty($primary_links)) { print "withprimary"; } if (!empty($secondary_links)) { print " withsecondary"; } ?> ">
        <?php if (!empty($primary_links)): ?>
          <div id="primary" class="clear-block">
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
          </div>
        <?php endif; ?>
        </div>

  
	  
	  <div id="slogan-search">
	  
	        <?php if (!empty($site_slogan)): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
			
			     <?php if (!empty($search_box)): ?>
        <div id="search-box"><?php print $search_box; ?></div>
      <?php endif; ?>
				 
				 </div>
				 
    <div id="container">
	
	     <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
	
        <div id="sidebar-left" class="column sidebar">
<?php print $left; ?>
        </div> <!-- /sidebar-left -->
 
        <div id="content">
        <div id="inner-content">
 <?php if (!empty($title)): ?><h2 class="title" id="page-title"><?php print $title; ?></h2><?php endif; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <?php if (!empty($help)): print $help; endif; ?>
      <?php print $content; ?>
      </div>
        </div> <!-- /content -->
  
	  
	   <?php if (!empty($right)): ?>
        <div id="sidebar-right" class="column sidebar">
          <?php print $right; ?>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>

    </div> <!-- /container -->
    

    <div id="footer-wrapper">

      <div id="footer">
	  
	    <?php if (!empty($footer_left)): ?>
        <div id="footer-left">
          <?php print $footer_left; ?>
        </div> <!-- /sidebar-left -->
      <?php endif; ?>
	  
	    <?php if (!empty($footer_middle)): ?>
        <div id="footer-middle">
          <?php print $footer_middle; ?>
        </div> <!-- /sidebar-middle -->
      <?php endif; ?>
	  
	  <?php if (!empty($footer_right)): ?>
        <div id="footer-right">
          <?php print $footer_right; ?>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>
	  
	    <?php if (!empty($secondary_links)): ?>
          <div id="secondary" class="clear-block">
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
          </div>
        <?php endif; ?>
	  
      <div id="footer-message">
  <?php print $footer_message; ?>
	 
</div>
      
		</div>
		
	
    </div> <!-- /footer-wrapper -->
    
    
    

  </div> <!-- /wrapper -->
<div id="theme-design">
<a href="http://www.suburban-glory.com/">London Web Design</a>
</div>


    <?php print $closure; ?>


</body>
</html>